﻿using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace subzicart.Controllers
{
    public class AdminController : Controller
    {
        // GET: Admin
        public ActionResult Index()
        {
            if (Request.Cookies["loginID"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }
        }
        public ActionResult logout()
        {

            Session["loginID"] = null;
            Session["userName"] = null;

            Response.Cookies["userName"].Expires = DateTime.Now.AddDays(-1);
            Response.Cookies["loginID"].Expires = DateTime.Now.AddDays(-1);
            return RedirectToAction("Login", "Home");
        }
        public ActionResult Insert_Category()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Insert_Category(Models.tblCategory modeldata)
        {

            if (ModelState.IsValid)
            {
                
                SqlParameter[] sp = new SqlParameter[6];
                sp[0] = new SqlParameter("@catName", modeldata.catName);
                sp[1] = new SqlParameter("@description", modeldata.description);
                sp[2] = new SqlParameter("@isactive", modeldata.isActive);
                sp[3] = new SqlParameter("@userid", Session["loginID"]);
                sp[4] = new SqlParameter("@action", "1");
                sp[5] = new SqlParameter("@remark", SqlDbType.VarChar, 100);
                sp[5].Direction = ParameterDirection.Output;
                int k = SqlHelper.ExecuteNonQuery(Connection.ConnectionString, CommandType.StoredProcedure, "sp_tblCategory", sp);
                if (k > 0)
                {

                    ViewBag.message = sp[5];
                    return RedirectToAction("ViewAllCategory", "Admin");
                }
                else
                {
                    ViewBag.message = sp[5].Value;
                }


              



            }
            return View();
        }

        public ActionResult update_Category(long mid)
        {
            using (var db = new Models.subzicartEntities())
            {
                //var response = db.Database.SqlQuery<Models.tblCategory>(Connection.ConnectionString, CommandType.StoredProcedure, "sp_tblCategory", new SqlParameter("@action", "5"), new SqlParameter("@catId", mid), new SqlParameter("@remark", SqlDbType.Char)).FirstOrDefault();
                DataSet ds= SqlHelper.ExecuteDataset(Connection.ConnectionString, CommandType.StoredProcedure, "sp_tblCategory", new SqlParameter("@action", "5"), new SqlParameter("@catId", mid), new SqlParameter("@remark", SqlDbType.Char));
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Models.tblCategory md = new Models.tblCategory();
                    md.catId = Convert.ToInt32(ds.Tables[0].Rows[0]["catId"]);
                    md.catName = ds.Tables[0].Rows[0]["catName"].ToString();
                    md.description = ds.Tables[0].Rows[0]["description"].ToString();
                    md.isActive = Convert.ToInt32(ds.Tables[0].Rows[0]["isActive"]);
                    ViewBag.isActive = Convert.ToInt32(ds.Tables[0].Rows[0]["isActive"]);

                    ViewBag.data = md;
                    return View(md);
                }
                else
                {
                    Session["Message1"] = "Record has an error";
                    return RedirectToAction("ViewAllCategory", "Admin");
                }
            }
           
        }
        [HttpPost]
        public ActionResult update_Category(Models.tblCategory modeldata)
        {

            if (ModelState.IsValid)
            {

                SqlParameter[] sp = new SqlParameter[7];
                sp[0] = new SqlParameter("@catName", modeldata.catName);
                sp[1] = new SqlParameter("@description", modeldata.description);
                sp[2] = new SqlParameter("@isactive", modeldata.isActive);
                sp[3] = new SqlParameter("@userid", Session["loginID"]);
                sp[4] = new SqlParameter("@action", "2");
                sp[5] = new SqlParameter("@catid", modeldata.catId);
                sp[6] = new SqlParameter("@remark", SqlDbType.VarChar, 100);
                sp[6].Direction = ParameterDirection.Output;
                int k = SqlHelper.ExecuteNonQuery(Connection.ConnectionString, CommandType.StoredProcedure, "sp_tblCategory", sp);
                if (k > 0)
                {

                    ViewBag.message = sp[5];
                    return RedirectToAction("ViewAllCategory", "Admin");
                }
                else
                {
                    ViewBag.message = sp[5].Value;
                }






            }
            return View();
        }

        public ActionResult ViewAllCategory()
        {
            try
            {
                ViewBag.message = Session["Message"];
            }
            catch (Exception ee){ }

           DataSet ds = SqlHelper.ExecuteDataset(Connection.ConnectionString, CommandType.StoredProcedure, "sp_tblCategory", new SqlParameter("@action", "3"),  new SqlParameter("@remark", SqlDbType.Char));
            ViewBag.category = ds.Tables[0];
            return View();
            
        }

        public ActionResult DeleteCategory(int mid)
        {
            if (ModelState.IsValid)
            {

                SqlParameter[] sp = new SqlParameter[3];
             
                sp[0] = new SqlParameter("@action", "6");
                sp[1] = new SqlParameter("@catid", mid);
                sp[2] = new SqlParameter("@remark", SqlDbType.VarChar, 200);
                sp[2].Direction = ParameterDirection.Output;
                int k = SqlHelper.ExecuteNonQuery(Connection.ConnectionString, CommandType.StoredProcedure, "sp_tblCategory", sp);
                if (k > 0)
                {

                    //ViewBag.message = sp[2];
                    Session["Message"] = sp[2].Value;
                    return RedirectToAction("ViewAllCategory", "Admin");
                }
                else
                {
                    Session["Message"] = sp[2].Value;
                 
                    return RedirectToAction("ViewAllCategory", "Admin");
                }






            }
            return View();

        }

        //------------------------------------SubCategory Operations-----------------------------

        public ActionResult Insert_SubCategory()
        {
            DataSet ds = SqlHelper.ExecuteDataset(Connection.ConnectionString, CommandType.StoredProcedure, "sp_tblCategory", new SqlParameter("@action", "3"), new SqlParameter("@remark", SqlDbType.Char));
            ViewBag.category = ds.Tables[0];
            return View();
        }
        [HttpPost]
        public ActionResult Insert_SubCategory(Models.tblSubCategory modeldata)
        {


            if (ModelState.IsValid)
            {

                SqlParameter[] sp = new SqlParameter[7];
                sp[0] = new SqlParameter("@subCatName", modeldata.subCatName);
                sp[1] = new SqlParameter("@catid", modeldata.catId);
                sp[2] = new SqlParameter("@description", modeldata.description);
                sp[3] = new SqlParameter("@isactive", modeldata.isActive);
                sp[4] = new SqlParameter("@userid", Session["loginID"]);
                sp[5] = new SqlParameter("@action", "1");
                sp[6] = new SqlParameter("@remark", SqlDbType.VarChar, 100);
                sp[6].Direction = ParameterDirection.Output;
                int k = SqlHelper.ExecuteNonQuery(Connection.ConnectionString, CommandType.StoredProcedure, "sp_tblSubCategory", sp);
                if (k > 0)
                {

                    ViewBag.message = sp[5];
                    return RedirectToAction("ViewAllCategory", "Admin");
                }
                else
                {
                    ViewBag.message = sp[5].Value;
                }






            }
            return View();
        }

        public ActionResult update_SubCategory(long mid)
        {
            DataSet ds1 = SqlHelper.ExecuteDataset(Connection.ConnectionString, CommandType.StoredProcedure, "sp_tblCategory", new SqlParameter("@action", "3"), new SqlParameter("@remark", SqlDbType.Char));
            ViewBag.category = ds1.Tables[0];
            using (var db = new Models.subzicartEntities())
            {
                //var response = db.Database.SqlQuery<Models.tblCategory>(Connection.ConnectionString, CommandType.StoredProcedure, "sp_tblCategory", new SqlParameter("@action", "5"), new SqlParameter("@catId", mid), new SqlParameter("@remark", SqlDbType.Char)).FirstOrDefault();
                DataSet ds = SqlHelper.ExecuteDataset(Connection.ConnectionString, CommandType.StoredProcedure, "sp_tblsubCategory", new SqlParameter("@action", "5"), new SqlParameter("@subcatId", mid), new SqlParameter("@remark", SqlDbType.Char));

                if (ds.Tables[0].Rows.Count > 0)
                {
                    Models.tblSubCategory md = new Models.tblSubCategory();
                    md.catId = Convert.ToInt32(ds.Tables[0].Rows[0]["catId"]);
                    md.subCatId = Convert.ToInt32(ds.Tables[0].Rows[0]["subCatId"]);
                    md.subCatName = ds.Tables[0].Rows[0]["subCatName"].ToString();
                    
                    md.description = ds.Tables[0].Rows[0]["description"].ToString();
                    md.isActive = Convert.ToInt32(ds.Tables[0].Rows[0]["isActive"]);
                    ViewBag.isActive = Convert.ToInt32(ds.Tables[0].Rows[0]["isActive"]);
                    ViewBag.categoryid = Convert.ToInt32(ds.Tables[0].Rows[0]["catId"]);
                   

                    ViewBag.data = md;

                    return View(md);
                }
                else
                {
                    Session["Message1"] = "Record has an error";
                    return RedirectToAction("ViewAllsubCategory", "Admin");
                }
            }

        }
        [HttpPost]
        public ActionResult update_SubCategory(Models.tblSubCategory modeldata)
        {

            if (ModelState.IsValid)
            {

                SqlParameter[] sp = new SqlParameter[8];
                sp[0] = new SqlParameter("@subCatId", modeldata.subCatId);
                sp[1] = new SqlParameter("@subCatName", modeldata.subCatName);
                sp[2] = new SqlParameter("@catid", modeldata.catId);
                sp[3] = new SqlParameter("@description", modeldata.description);
                sp[4] = new SqlParameter("@isactive", modeldata.isActive);
                sp[5] = new SqlParameter("@userid", Session["loginID"]);
                sp[6] = new SqlParameter("@action", "2");
                sp[7] = new SqlParameter("@remark", SqlDbType.VarChar, 100);
                sp[7].Direction = ParameterDirection.Output;
                int k = SqlHelper.ExecuteNonQuery(Connection.ConnectionString, CommandType.StoredProcedure, "sp_tblSubCategory", sp);
                if (k > 0)
                {

                    ViewBag.message = sp[5].Value;
                    return RedirectToAction("ViewAllsubCategory", "Admin");
                }
                else
                {
                    ViewBag.message = sp[5].Value;
                }



            }
            return View();
        }

        public ActionResult ViewAllSubCategory()
        {
            try
            {
                ViewBag.message = Session["Message1"];
            }
            catch (Exception ee) { }

            DataSet ds = SqlHelper.ExecuteDataset(Connection.ConnectionString, CommandType.StoredProcedure, "sp_tblSubCategory", new SqlParameter("@action", "3"), new SqlParameter("@remark", SqlDbType.Char));
            ViewBag.category = ds.Tables[0];
            return View();

        }

        public ActionResult DeleteSubCategory(int mid)
        {
            if (ModelState.IsValid)
            {

                SqlParameter[] sp = new SqlParameter[3];

                sp[0] = new SqlParameter("@action", "6");
                sp[1] = new SqlParameter("@subcatid", mid);
                sp[2] = new SqlParameter("@remark", SqlDbType.VarChar, 200);
                sp[2].Direction = ParameterDirection.Output;
                int k = SqlHelper.ExecuteNonQuery(Connection.ConnectionString, CommandType.StoredProcedure, "sp_tblSubCategory", sp);
                if (k > 0)
                {

                    //ViewBag.message = sp[2];
                    Session["Message1"] = sp[2].Value;
                    return RedirectToAction("ViewAllsubCategory", "Admin");
                }
                else
                {
                    Session["Message1"] = sp[2].Value;

                    return RedirectToAction("ViewAllsubCategory", "Admin");
                }






            }
            return View();

        }

        //---------------------------Currency Master-------------------------


        public ActionResult Insert_Currency()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Insert_Currency(Models.tblCurrencyMaster modeldata)
        {

            if (ModelState.IsValid)
            {

                SqlParameter[] sp = new SqlParameter[7];
                sp[0] = new SqlParameter("@currency", modeldata.currency);
                sp[1] = new SqlParameter("@value", modeldata.value);
                sp[2] = new SqlParameter("@rateinUsd", modeldata.rateInUSD);
                sp[3] = new SqlParameter("@active", modeldata.active);
                sp[4] = new SqlParameter("@userid", Session["loginID"]);
                sp[5] = new SqlParameter("@action", "1");
                sp[6] = new SqlParameter("@remark", SqlDbType.VarChar, 100);
                sp[6].Direction = ParameterDirection.Output;
                int k = SqlHelper.ExecuteNonQuery(Connection.ConnectionString, CommandType.StoredProcedure, "sp_tblCurrencymaster", sp);
                if (k > 0)
                {

                    ViewBag.message = sp[6].Value;
                    return RedirectToAction("ViewAllCurrency", "Admin");
                }
                else
                {
                    ViewBag.message = sp[6].Value;
                }






            }
            return View();
        }

        public ActionResult update_Currency(long mid)
        {
            using (var db = new Models.subzicartEntities())
            {
                //var response = db.Database.SqlQuery<Models.tblCategory>(Connection.ConnectionString, CommandType.StoredProcedure, "sp_tblCategory", new SqlParameter("@action", "5"), new SqlParameter("@catId", mid), new SqlParameter("@remark", SqlDbType.Char)).FirstOrDefault();
                DataSet ds = SqlHelper.ExecuteDataset(Connection.ConnectionString, CommandType.StoredProcedure, "sp_tblCurrencymaster", new SqlParameter("@action", "5"), new SqlParameter("@cId", mid), new SqlParameter("@remark", SqlDbType.Char));
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Models.tblCurrencyMaster md = new Models.tblCurrencyMaster();
                    md.cid = Convert.ToInt32(ds.Tables[0].Rows[0]["cid"]);
                    md.currency = ds.Tables[0].Rows[0]["currency"].ToString();
                    md.value = Convert.ToDouble(ds.Tables[0].Rows[0]["value"].ToString());
                    md.rateInUSD = Convert.ToDouble(ds.Tables[0].Rows[0]["rateInUSD"].ToString());
                    md.active = Convert.ToInt32(ds.Tables[0].Rows[0]["active"]);
                    ViewBag.isActive = Convert.ToInt32(ds.Tables[0].Rows[0]["active"]);

                    ViewBag.data = md;
                    return View(md);
                }
                else
                {
                    Session["Message2"] = "Record has an error";
                    return RedirectToAction("ViewAllCurrency", "Admin");
                }
            }

        }
        [HttpPost]
        public ActionResult update_Currency(Models.tblCurrencyMaster modeldata)
        {

            if (ModelState.IsValid)
            {

                SqlParameter[] sp = new SqlParameter[8];
                sp[0] = new SqlParameter("@cid", modeldata.cid);
                sp[1] = new SqlParameter("@currency", modeldata.currency);
                sp[2] = new SqlParameter("@value", modeldata.value);
                sp[3] = new SqlParameter("@rateinUsd", modeldata.rateInUSD);
                sp[4] = new SqlParameter("@active", modeldata.active);
                sp[5] = new SqlParameter("@userid", Session["loginID"]);
                sp[6] = new SqlParameter("@action", "2");
                sp[7] = new SqlParameter("@remark", SqlDbType.VarChar, 100);
                sp[7].Direction = ParameterDirection.Output;
                int k = SqlHelper.ExecuteNonQuery(Connection.ConnectionString, CommandType.StoredProcedure, "sp_tblCurrencymaster", sp);
                if (k > 0)
                {

                    ViewBag.message = sp[7].Value;
                    return RedirectToAction("ViewAllCurrency", "Admin");
                }
                else
                {
                    ViewBag.message = sp[7].Value;
                }



            }
            return View();
        }

        public ActionResult ViewAllCurrency()
        {
            try
            {
                ViewBag.message = Session["Message2"];
            }
            catch (Exception ee) { }

            DataSet ds = SqlHelper.ExecuteDataset(Connection.ConnectionString, CommandType.StoredProcedure, "sp_tblCurrencymaster", new SqlParameter("@action", "3"), new SqlParameter("@remark", SqlDbType.Char));
            ViewBag.category = ds.Tables[0];
            return View();

        }

        public ActionResult DeleteCurrency(int mid)
        {
            if (ModelState.IsValid)
            {

                SqlParameter[] sp = new SqlParameter[3];

                sp[0] = new SqlParameter("@action", "6");
                sp[1] = new SqlParameter("@cid", mid);
                sp[2] = new SqlParameter("@remark", SqlDbType.VarChar, 200);
                sp[2].Direction = ParameterDirection.Output;
                int k = SqlHelper.ExecuteNonQuery(Connection.ConnectionString, CommandType.StoredProcedure, "sp_tblCurrencymaster", sp);
                if (k > 0)
                {

                    //ViewBag.message = sp[2];
                    Session["Message2"] = sp[2].Value;
                    return RedirectToAction("ViewAllCurrency", "Admin");
                }
                else
                {
                    Session["Message2"] = sp[2].Value;

                    return RedirectToAction("ViewAllCurrency", "Admin");
                }






            }
            return View();

        }




        //---------------------------Brand Prefixes-------------------------


        public ActionResult Insert_brandprefix()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Insert_brandprefix(Models.tblBrandPrefix modeldata)
        {

            if (ModelState.IsValid)
            {

                SqlParameter[] sp = new SqlParameter[8];
                sp[0] = new SqlParameter("@brand_name", modeldata.brand_name);
                sp[1] = new SqlParameter("@brand_prefix", modeldata.brand_prefix);
                sp[2] = new SqlParameter("@sku_start_range", modeldata.sku_start_range);
                sp[3] = new SqlParameter("@sku_end_range", modeldata.sku_end_range);
                sp[4] = new SqlParameter("@active", modeldata.active);
                sp[5] = new SqlParameter("@userid", Session["loginID"]);
                sp[6] = new SqlParameter("@action", "1");
                sp[7] = new SqlParameter("@remark", SqlDbType.VarChar, 100);
                sp[7].Direction = ParameterDirection.Output;
                int k = SqlHelper.ExecuteNonQuery(Connection.ConnectionString, CommandType.StoredProcedure, "sp_tblBrandPrefixes", sp);
                if (k > 0)
                {

                    ViewBag.message = sp[7].Value;
                    return RedirectToAction("ViewAllbrandprefix", "Admin");
                }
                else
                {
                    ViewBag.message = sp[7].Value;
                }






            }
            return View();
        }

        public ActionResult update_brandprefix(long mid)
        {
            using (var db = new Models.subzicartEntities())
            {
                //var response = db.Database.SqlQuery<Models.tblCategory>(Connection.ConnectionString, CommandType.StoredProcedure, "sp_tblCategory", new SqlParameter("@action", "5"), new SqlParameter("@catId", mid), new SqlParameter("@remark", SqlDbType.Char)).FirstOrDefault();
                DataSet ds = SqlHelper.ExecuteDataset(Connection.ConnectionString, CommandType.StoredProcedure, "sp_tblBrandPrefixes", new SqlParameter("@action", "5"), new SqlParameter("@brand_id", mid), new SqlParameter("@remark", SqlDbType.Char));
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Models.tblBrandPrefix md = new Models.tblBrandPrefix();
                    md.brand_id = Convert.ToInt32(ds.Tables[0].Rows[0]["brand_id"]);
                    md.brand_name = ds.Tables[0].Rows[0]["brand_name"].ToString();
                    md.brand_prefix = ds.Tables[0].Rows[0]["brand_prefix"].ToString();
                    md.sku_start_range = ds.Tables[0].Rows[0]["sku_start_range"].ToString();
                    md.sku_end_range = ds.Tables[0].Rows[0]["sku_end_range"].ToString();
                    md.active = Convert.ToInt32(ds.Tables[0].Rows[0]["active"]);
                    ViewBag.isActive = Convert.ToInt32(ds.Tables[0].Rows[0]["active"]);

                    ViewBag.data = md;
                    return View(md);
                }
                else
                {
                    Session["Message3"] = "Record has an error";
                    return RedirectToAction("ViewAllbrandprefix", "Admin");
                }
            }

        }
        [HttpPost]
        public ActionResult update_brandprefix(Models.tblBrandPrefix modeldata)
        {

            if (ModelState.IsValid)
            {

                SqlParameter[] sp = new SqlParameter[9];
                sp[0] = new SqlParameter("@brand_name", modeldata.brand_name);
                sp[1] = new SqlParameter("@brand_prefix", modeldata.brand_prefix);
                sp[2] = new SqlParameter("@sku_start_range", modeldata.sku_start_range);
                sp[3] = new SqlParameter("@sku_end_range", modeldata.sku_end_range);
                
                sp[4] = new SqlParameter("@brand_id", modeldata.brand_id);

                sp[5] = new SqlParameter("@active", modeldata.active);
                sp[6] = new SqlParameter("@userid", Session["loginID"]);
                sp[7] = new SqlParameter("@action", "2");
                sp[8] = new SqlParameter("@remark", SqlDbType.VarChar, 100);
                sp[8].Direction = ParameterDirection.Output;
                int k = SqlHelper.ExecuteNonQuery(Connection.ConnectionString, CommandType.StoredProcedure, "sp_tblBrandPrefixes", sp);
                if (k > 0)
                {

                    ViewBag.message = sp[8].Value;
                    return RedirectToAction("ViewAllbrandprefix", "Admin");
                }
                else
                {
                    ViewBag.message = sp[8].Value;
                }




            }
            return View();
        }

        public ActionResult ViewAllbrandprefix()
        {
            try
            {
                ViewBag.message = Session["Message3"];
            }
            catch (Exception ee) { }

            DataSet ds = SqlHelper.ExecuteDataset(Connection.ConnectionString, CommandType.StoredProcedure, "sp_tblBrandPrefixes", new SqlParameter("@action", "3"), new SqlParameter("@remark", SqlDbType.Char));
            ViewBag.category = ds.Tables[0];
            return View();

        }

        public ActionResult Deletebrandprefix(int mid)
        {
            if (ModelState.IsValid)
            {

                SqlParameter[] sp = new SqlParameter[3];

                sp[0] = new SqlParameter("@action", "6");
                sp[1] = new SqlParameter("@brand_id", mid);
                sp[2] = new SqlParameter("@remark", SqlDbType.VarChar, 200);
                sp[2].Direction = ParameterDirection.Output;
                int k = SqlHelper.ExecuteNonQuery(Connection.ConnectionString, CommandType.StoredProcedure, "sp_tblBrandPrefixes", sp);
                if (k > 0)
                {

                    //ViewBag.message = sp[2];
                    Session["Message3"] = sp[2].Value;
                    return RedirectToAction("ViewAllbrandprefix", "Admin");
                }
                else
                {
                    Session["Message3"] = sp[2].Value;

                    return RedirectToAction("ViewAllbrandprefix", "Admin");
                }






            }
            return View();

        }




        //---------------------------Mesaurement Master-------------------------


        public ActionResult Insert_mesaurementmaster()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Insert_mesaurementmaster(Models.tblMeasurementMaster modeldata)
        {

            if (ModelState.IsValid)
            {

                SqlParameter[] sp = new SqlParameter[8];
                sp[0] = new SqlParameter("@unit_type", modeldata.unit_type);
                sp[1] = new SqlParameter("@unit_value", modeldata.unit_value);
                sp[2] = new SqlParameter("@eq_unit_type", modeldata.eq_unit_type);
                sp[3] = new SqlParameter("@eq_unit_value", modeldata.eq_unit_value);
                sp[4] = new SqlParameter("@active", modeldata.active);
                sp[5] = new SqlParameter("@userid", Session["loginID"]);
                sp[6] = new SqlParameter("@action", "1");
                sp[7] = new SqlParameter("@remark", SqlDbType.VarChar, 100);
                sp[7].Direction = ParameterDirection.Output;
                int k = SqlHelper.ExecuteNonQuery(Connection.ConnectionString, CommandType.StoredProcedure, "sp_tblMeasurementMaster", sp);
                if (k > 0)
                {

                    ViewBag.message = sp[7].Value;
                    return RedirectToAction("ViewAllmesaurementmaster", "Admin");
                }
                else
                {
                    ViewBag.message = sp[7].Value;
                }






            }
            return View();
        }

        public ActionResult update_mesaurementmaster(long mid)
        {
            using (var db = new Models.subzicartEntities())
            {
                //var response = db.Database.SqlQuery<Models.tblCategory>(Connection.ConnectionString, CommandType.StoredProcedure, "sp_tblCategory", new SqlParameter("@action", "5"), new SqlParameter("@catId", mid), new SqlParameter("@remark", SqlDbType.Char)).FirstOrDefault();
                DataSet ds = SqlHelper.ExecuteDataset(Connection.ConnectionString, CommandType.StoredProcedure, "sp_tblMeasurementMaster", new SqlParameter("@action", "5"), new SqlParameter("@m_id", mid), new SqlParameter("@remark", SqlDbType.Char));
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Models.tblMeasurementMaster md = new Models.tblMeasurementMaster();
                    md.m_id = Convert.ToInt32(ds.Tables[0].Rows[0]["m_id"]);
                    md.unit_type = ds.Tables[0].Rows[0]["unit_type"].ToString();
                    md.unit_value = Convert.ToDouble(ds.Tables[0].Rows[0]["unit_value"].ToString());
                    md.eq_unit_type = ds.Tables[0].Rows[0]["eq_unit_type"].ToString();
                    md.eq_unit_value = Convert.ToDouble(ds.Tables[0].Rows[0]["eq_unit_value"].ToString());
                    md.active = Convert.ToInt32(ds.Tables[0].Rows[0]["active"]);
                    ViewBag.isActive = Convert.ToInt32(ds.Tables[0].Rows[0]["active"]);

                    ViewBag.data = md;
                    return View(md);
                }
                else
                {
                    Session["Message3"] = "Record has an error";
                    return RedirectToAction("ViewAllmesaurementmaster", "Admin");
                }
            }

        }
        [HttpPost]
        public ActionResult update_mesaurementmaster(Models.tblMeasurementMaster modeldata)
        {

            if (ModelState.IsValid)
            {

                SqlParameter[] sp = new SqlParameter[9];
                sp[0] = new SqlParameter("@unit_type", modeldata.unit_type);
                sp[1] = new SqlParameter("@unit_value", modeldata.unit_value);
                sp[2] = new SqlParameter("@eq_unit_type", modeldata.eq_unit_type);
                sp[3] = new SqlParameter("@eq_unit_value", modeldata.eq_unit_value);

                sp[4] = new SqlParameter("@m_id", modeldata.m_id);

                sp[5] = new SqlParameter("@active", modeldata.active);
                sp[6] = new SqlParameter("@userid", Session["loginID"]);
                sp[7] = new SqlParameter("@action", "2");
                sp[8] = new SqlParameter("@remark", SqlDbType.VarChar, 100);
                sp[8].Direction = ParameterDirection.Output;
                int k = SqlHelper.ExecuteNonQuery(Connection.ConnectionString, CommandType.StoredProcedure, "sp_tblMeasurementMaster", sp);
                if (k > 0)
                {

                    ViewBag.message = sp[8].Value;
                    return RedirectToAction("ViewAllmesaurementmaster", "Admin");
                }
                else
                {
                    ViewBag.message = sp[8].Value;
                }




            }
            return View();
        }

        public ActionResult ViewAllmesaurementmaster()
        {
            try
            {
                ViewBag.message = Session["Message3"];
            }
            catch (Exception ee) { }

            DataSet ds = SqlHelper.ExecuteDataset(Connection.ConnectionString, CommandType.StoredProcedure, "sp_tblMeasurementMaster", new SqlParameter("@action", "3"), new SqlParameter("@remark", SqlDbType.Char));
            ViewBag.category = ds.Tables[0];
            return View();

        }

        public ActionResult Deletemesaurementmaster(int mid)
        {
            if (ModelState.IsValid)
            {

                SqlParameter[] sp = new SqlParameter[3];

                sp[0] = new SqlParameter("@action", "6");
                sp[1] = new SqlParameter("@m_id", mid);
                sp[2] = new SqlParameter("@remark", SqlDbType.VarChar, 200);
                sp[2].Direction = ParameterDirection.Output;
                int k = SqlHelper.ExecuteNonQuery(Connection.ConnectionString, CommandType.StoredProcedure, "sp_tblMeasurementMaster", sp);
                if (k > 0)
                {

                    //ViewBag.message = sp[2];
                    Session["Message3"] = sp[2].Value;
                    return RedirectToAction("ViewAllmesaurementmaster", "Admin");
                }
                else
                {
                    Session["Message3"] = sp[2].Value;

                    return RedirectToAction("ViewAllmesaurementmaster", "Admin");
                }






            }
            return View();

        }


    }
}